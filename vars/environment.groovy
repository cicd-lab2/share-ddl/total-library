def call(){
    env.Agent = "ssh-agent"
    env.Sonar_config = "Sonar"
    env.Remote_User = "root"
    env.Remote_Host = "192.168.20.30"
    // dev
    env.Container_dev = "petclinic_dev"
    env.Image_dev = "192.168.20.20:8088/petclinic-dl:latest"
    env.Repo_url_dev = "192.168.20.20:8088"
    env.Port_dev = "8888"
    // product
    env.Container_product = "petclinic_product"
    env.Image_product = "192.168.20.20:8089/petclinic-dl:latest"
    env.Repo_url_product = "192.168.20.20:8089"
    env.Port_product = "9999"
    // build image
    env.Image_name = "petclinic-dl"
    // sonar scan
    env.InstallationName = 'SonarQube Server'
    env.CredentialsId = "${env.credentialsId_sonar}"
    


}
// def danglingImages = sh(script: 'docker images -f dangling=true -q',returnStdout: true).trim()
