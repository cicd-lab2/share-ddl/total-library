def call(){
    node {
        stage('checkout scm'){
            deleteDir()
            checkout scm
        }
        environment()
        // stage('maven complite'){
        //     maven_build()
        // }
        // stage('maven test'){
        //     maven_test()
        // }
        // stage('Scan Sonarqube'){
        //     sonar()
        // }
        if (env.BRANCH_NAME ==~ /(?i)^uat/) {
            stage('Build Image DEV and push to nexus'){
                docker_image_dev()
                docker_image_product()
            }
            // stage('Build Image PRODUCT and push to nexus'){
            //     docker_image_product()
            // }
            // stage('Deploy container in Product environment (UAT)') {
            // when {
            //     expression {
            //         return env.BRANCH_NAME ==~ /(?i)^uat/
            //     }
            // }
            stage('Deploy container in Dev environment') {
                script {
                    sh "echo ${env.BRANCH_NAME} "
                    deploy_dev()
                }
            }
            
            // stage('Deploy container in Product environment'){ 
            //     catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
            //         deploy_dev()
            //     }
            // }
            stage('Deploy container in Product environment'){
                deploy_product()
            }
        }else {
            stage('branch name dev') {
                script {
                    sh "echo 'build in ${env.BRANCH_NAME}' "
                }
            }
        }
    }
}
